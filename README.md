# job-search

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Chapter 1 and 2
### Notes related to project customization

This project was created in Visual Studio code and PhpStorm both containing their respective configuration files. I won't talk about what was configured for PhpStorm, instead my focus will be on VS Code.

The following steps was taken by me for chapters one and two.

1. `vue create job-search`
2. Select: Manual/custom installation - leave defaults - only add unit testing
3. choose jest and prettier
4. Make sure VS code has prettier, vuetur and linter plugins installed
5. Cmd/Shift P search for tab size: set **tab size** from 4 to 2
6. wait, just paste the following in jest.config.js and .vscode/settings.json

### jest.config.js
testMatch will look in the test folder and find any file that contains spec or test - compiles and executes the test.

```json
// jest.config.js
module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  testMatch: ["**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)"],
};

```

### visual studio code settings
Choose Workspace instead of the global User in settings so that you don't affect the overall project settings. You can paste this code into the newly created settings.json in the .vscode folder.

```js
// .vscode/settings.json
{
  "editor.formatOnSave": true,
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.tabSize": 2,
  "eslint.validate": [ "javascript", "vue" ],
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  }
}

```
### Linter settings
The most important thing here is to fix the test. Coping the test configuration from jest.config.js will ensure linter is aware of where to run tests.

The other thing is we changed the vue3- from vue3-essential to vue3-recommended

```json
// .eslintrc.js
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/vue3-recommended",
    "eslint:recommended",
    "plugin:prettier/recommended",
  ],
  parserOptions: {
    parser: "@babel/eslint-parser",
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
  },
  overrides: [
    {
      files: ["**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)"],
      env: {
        jest: true,
      },
    },
  ],
};
```
