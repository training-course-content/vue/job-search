import { nextTick } from "vue";
import { mount } from "@vue/test-utils";
import Headline from "@/components/jobsearch/HeadLine";

describe("Headline", () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.useRealTimers();
  });

  it("displays introductory action verb", () => {
    const wrapper = mount(Headline);
    const actionPhrase = wrapper.find("[data-test='action-phrase']");
    expect(actionPhrase.text()).toBe("Build for everyone");
  });

  it("swaps action verb after first interval", async () => {
    const wrapper = mount(Headline);
    jest.runOnlyPendingTimers();
    // console.log(wrapper.vm); // what a component is vue model
    // console.log(wrapper.vm.action);
    // we need to simpulate a refresh, not a page refresh but a simulation
    // of the action text being updated every 3 seconds and changing to the
    // corresponding next item in the list. NextTick
    await nextTick();
    const actionPhrase = wrapper.find("[data-test='action-phrase']");
    expect(actionPhrase.text()).toBe("Create for everyone");
  });
});
