import { mount } from "@vue/test-utils";

import JobSearchForm from "@/components/jobsearch/JobSearchForm";

describe("JobSearchForm", () => {
  describe("when user submits form", () => {
    it("directs user to job results page with user's search parameters", async () => {
      const push = jest.fn();
      const $router = { push };

      const wrapper = mount(JobSearchForm, {
        attachTo: document.body,
        global: {
          mocks: {
            $router,
          },
          stubs: {
            FontAwesomeIcon: true,
          },
        },
      });

      await wrapper.find("[data-test='role-input']").setValue("Vue Developer");

      await wrapper.find("[data-test='location-input']").setValue("Seattle");

      await wrapper.find("[data-test='form-submit-button']").trigger("click");

      // expect(push).toHaveBeenCalled({
      //   name: "JobResults",
      //   query: {
      //     role: "Vue Developer",
      //     location: "Seattle",
      //   },
      // });
    });
  });
});
