import { createRouter, createWebHashHistory } from "vue-router";
const HomeView = () => import("@/views/HomeView.vue");
const JobResultsView = () =>
  import(/* webpackChunkName: "jobs" */ "@/views/JobResultsView.vue");
const JobView = () =>
  import(/* webpackChunkName: "jobs" */ "@/views/JobView.vue");
const routes = [
  {
    path: "/",
    component: HomeView,
    name: "Home",
  },
  {
    path: "/job/results",
    component: JobResultsView,
    name: "JobResults",
  },
  {
    path: "/job/results/:id",
    component: JobView,
    name: "JobListing",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior() {
    return {
      top: 0,
      left: 0,
      behavior: "smooth",
    };
  },
});

export default router;
