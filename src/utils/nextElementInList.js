const nextElementInList = (list, value) => {
  const currentActionIndex = list.indexOf(value);
  const nextValueIndex = (currentActionIndex + 1) % list.length; // ensure we don't escape the array
  const nextAction = list[nextValueIndex];
  return nextAction;
};
export default nextElementInList;
